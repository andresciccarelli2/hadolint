FROM debian:stretch-slim
RUN apt-get update  -y \
    && apt-get install wget -y \
    && wget -O /bin/hadolint https://github.com/hadolint/hadolint/releases/download/v1.16.3/hadolint-Linux-x86_64 \
    && chmod +x /bin/hadolint 
CMD ["/bin/hadolint", "-"]
